; Qn 1 - Lab 4 & 5

lxi b,2000h ;starting ptr
lxi d,3000h ; destination ptr
mvi h,05h ; loop ctrl

loop: nop
ldax b
stax d
inx b
inx d
dcr h
jnz loop

hlt